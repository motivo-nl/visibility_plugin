<?php

use Motivo\Liberiser\Visibility\Models\Visibility;
use Motivo\Liberiser\Visibility\Mixins\VisibilityMixins;
use Motivo\Liberiser\Visibility\Http\Requests\StoreVisibilityRequest;
use Motivo\Liberiser\Visibility\Http\Requests\UpdateVisibilityRequest;

return [
    'class' => Visibility::class,
    'mixins' => VisibilityMixins::class,
    'store_request' => StoreVisibilityRequest::class,
    'update_request' => UpdateVisibilityRequest::class,
];
