<?php

namespace Motivo\Liberiser\Visibility\Mixins;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Motivo\Liberiser\Visibility\Models\Visibility;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class VisibilityMixins
{
    public function visibility(): Closure
    {
        return function (): MorphOne {
            return $this->morphOne(Visibility::class, 'model');
        };
    }

    public static function visible(): Closure
    {
        return function (): Builder {
            $table = self::getTableName();
            $class = self::class;

            return self::select("{$table}.*")
                ->leftJoin('liberiser_visibilities', function (JoinClause $join) use ($table, $class) {
                    $join->on('liberiser_visibilities.model_id', '=', "{$table}.id")
                         ->where('liberiser_visibilities.model_type', '=', (new $class)->getMorphClass());
                })
                ->where(function (Builder $filter) use ($table) {
                    $filter->whereNull('liberiser_visibilities.id')
                        ->orWhere(function (Builder $subQuery) {
                            $subQuery->where('liberiser_visibilities.visible', '=', true)
                                ->where(function (Builder $languageActive) {
                                    $languageActive->orWhereNull('liberiser_visibilities.visible_languages')
                                        ->orWhere('liberiser_visibilities.visible_languages', 'LIKE', '%'.app()->getLocale().'%');
                                })
                                ->where(function (Builder $start) {
                                    $start->orWhereNull('liberiser_visibilities.from')
                                        ->orWhere('liberiser_visibilities.from', '<', Carbon::now()->toDateString());
                                })
                                ->where(function (Builder $end) {
                                    $end->orWhereNull('liberiser_visibilities.until')
                                        ->orWhere('liberiser_visibilities.until', '>', Carbon::now()->toDateString());
                                });
                        });
                });
        };
    }

    public function isVisible(): Closure
    {
        return function (): bool {
            return (bool) self::visible()->where(self::getTableName().'.id', '=', $this->id)->first();
        };
    }

    public function visibleLanguages(): Closure
    {
        return function (): array {
            if (isset($this->visibility) && isset($this->visibility->visible_languages)) {
                return explode(',', $this->visibility->visible_languages);
            }

            return [];
        };
    }
}
