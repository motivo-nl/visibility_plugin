<?php

namespace Motivo\Liberiser\Visibility\Validations;

use Illuminate\Support\Facades\App;
use Illuminate\Validation\Validator;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Interfaces\ValidationInterface;

class PageValidation implements ValidationInterface
{
    /** @var string */
    private $locale;

    /** @var string|null */
    private $title;

    /** @var int */
    private $visibility;

    /** @var array|null */
    private $visibility_per_language;

    public function before(LiberiserRequest $request): void
    {
        $this->locale = $request->input('locale', App::getLocale());
        $this->title = $request->input('title', null);
        $this->visibility = (int) $request->input('visibility_visible', 0);
        $this->visibility_per_language = $request->input('visibility_visible_languages', null);

        if ($this->visibility_per_language === 'null') {
            $this->visibility_per_language = null;
        }
    }

    public function rules(): ?array
    {
        return [
            'title' => $this->getTitleRules(),
        ];
    }

    private function getTitleRules(): array
    {
        $required = $this->visibility === 1 &&
            (
                ! isset($this->visibility_per_language) ||
                (isset($this->visibility_per_language) && in_array($this->locale, $this->visibility_per_language))
            );

        if ($required || $this->locale === App::getLocale()) {
            return ['required'];
        }

        return ['nullable'];
    }

    public function after(Validator $validator): void
    {
    }
}
