<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiberiserVisibilityTable extends Migration
{
    public function up(): void
    {
        Schema::create('liberiser_visibilities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->morphs('model');

            $table->boolean('visible');
            $table->string('visible_languages')->nullable()->default(null);

            $table->dateTime('from')->nullable()->default(null);
            $table->dateTime('until')->nullable()->default(null);

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('liberiser_visibilities');
    }
}
