<?php

namespace Motivo\Liberiser\Visibility\Http\Requests;

use Motivo\Liberiser\Base\Models\BaseModel;
use Motivo\Liberiser\Visibility\Models\Visibility;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\UpdateLiberiserRequest;

class UpdateVisibilityRequest extends UpdateLiberiserRequest
{
    protected $model = Visibility::class;

    public static function getRules(LiberiserRequest $request, ?BaseModel $entity): array
    {
        return [
            'visibility_visible' => [
                'nullable',
            ],
            'visibility_from' => [
                'nullable',
                'date',
            ],
            'visibility_until' => [
                'nullable',
                'date',
            ],
        ];
    }
}
