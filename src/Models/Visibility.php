<?php

namespace Motivo\Liberiser\Visibility\Models;

use Illuminate\Support\Facades\Schema;
use Motivo\Liberiser\Base\Models\Language;
use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Visibility extends BaseModel
{
    protected $table = 'liberiser_visibilities';

    protected $fillable = ['visible', 'visible_languages', 'from', 'until'];

    protected $casts = [
        'visible' => 'bool',
    ];

    protected $dates = [
        'from',
        'until',
    ];

    public function model(): MorphTo
    {
        return $this->morphTo('model');
    }

    public static function getModuleName(): string
    {
        return 'visibility';
    }

    public static function getComponentFields(?BaseModel $model = null): array
    {
        $languages = Language::active()->mapWithKeys(function (Language $language) {
            return [$language['shortcode'] => $language->label];
        });

        return [
            [
                'name' => 'visibility',
                'relation' => static::getModuleName(),
                'type' => 'liberiser_relation',
                'relationFields' => [
                    [
                        'name' => 'visible',
                        'type' => 'visibility_per_language',
                        'label' => ucfirst(trans('LiberiserVisibility::visibility.fields.visibility')),
                        'languages' => $languages,
                        'model' => self::class,
                        'entity' => 'visibility',
                        'morph' => true,
                    ],
                    [
                        'name' => 'from',
                        'type' => 'datetime_picker',
                        'label' => ucfirst(trans('LiberiserVisibility::visibility.fields.from')),
                        'datetime_picker_options' => [
                            'format' => 'DD/MM/YYYY HH:mm',
                            'language' => app()->getLocale(),
                        ],
                        'allows_null' => true,
                        'model' => self::class,
                        'entity' => 'visibility',
                        'morph' => true,
                        'hide_when' => [
                            'field' => 'visibility_visible',
                            'value' => 0,
                        ],
                    ],
                    [
                        'name' => 'until',
                        'type' => 'datetime_picker',
                        'label' => ucfirst(trans('LiberiserVisibility::visibility.fields.until')),
                        'datetime_picker_options' => [
                            'format' => 'DD/MM/YYYY HH:mm',
                            'language' => app()->getLocale(),
                        ],
                        'allows_null' => true,
                        'model' => self::class,
                        'entity' => 'visibility',
                        'morph' => true,
                        'hide_when' => [
                            'field' => 'visibility_visible',
                            'value' => 0,
                        ],
                    ],
                ],
            ],
        ];
    }

    public static function getComponentColumns(): array
    {
        return [];
    }

    public static function getComponentRelations(): array
    {
        if (! Schema::hasTable('liberiser_visibilities')) {
            return [];
        }

        return ['visibility'];
    }

    public function getDisplayNameAttribute(): string
    {
        return $this->id;
    }

    public function getTextAttribute(): string
    {
        $visible = $this->attributes['visible'];

        $text = $visible ? ucfirst(trans('LiberiserVisibility::visibility.yes')) : ucfirst(trans('LiberiserVisibility::visibility.no'));

        if ($visible && ($this->from || $this->until)) {
            $text .= ', ';
        }

        if ($visible && $this->from) {
            $text .= ' '.trans('LiberiserVisibility::visibility.from', ['date' => $this->from->format('d-m-Y')]);
        }

        if ($visible && $this->until) {
            $text .= ' '.trans('LiberiserVisibility::visibility.until', ['date' => $this->until->format('d-m-Y')]);
        }

        return $text;
    }
}
