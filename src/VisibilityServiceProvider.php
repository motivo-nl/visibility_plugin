<?php

namespace Motivo\Liberiser\Visibility;

use Illuminate\Support\ServiceProvider;
use Motivo\Liberiser\Base\Validation\Validation;
use Motivo\Liberiser\Visibility\Validations\PageValidation;
use Motivo\Liberiser\Pages\Http\Controllers\PagesCrudController;

class VisibilityServiceProvider extends ServiceProvider
{
    /** @var string */
    protected $publishablePrefix = 'liberiser';

    /** @var string */
    protected $packageNamespace = 'LiberiserVisibility';

    public function register(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        $this->loadViewsFrom(__DIR__.'/../resources/views', $this->packageNamespace);

        $this->mergeConfigFrom(__DIR__.'/../config/visibility.php', 'liberiser.visibility');
    }

    public function boot(): void
    {
        $this->registerPublishing();

        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', $this->packageNamespace);

        Validation::addValidation(PagesCrudController::class, [
           class_basename(PageValidation::class) => new PageValidation(),
        ]);
    }

    private function registerPublishing(): void
    {
        $this->publishes([
            __DIR__.'/../config/visibility.php' => config_path('liberiser/visibility.php'),
        ], 'liberiser:config');

        $this->publishes([
            __DIR__.'/../resources/views/vendor/backpack/' => resource_path('views/vendor/backpack'),
        ], $this->publishablePrefix.':backpack');
    }
}
