<?php

namespace Tests\Feature;

use Str;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Http\Response;
use App\Http\Middleware\CheckIfAdmin;
use Motivo\Liberiser\Pages\Models\Page;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Foundation\Testing\WithFaker;
use Motivo\Liberiser\Base\Models\Permission;

class VisibilityTest extends TestCase
{
    use WithFaker;

    private const LOCALE_NL = 'nl';

    private const LOCALE_EN = 'en';

    /** @var User */
    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->withoutMiddleware(VerifyCsrfToken::class);
        $this->withoutMiddleware(CheckIfAdmin::class);

        $this->be(new User([
            'id' => 1,
            'first_name' => 'User',
            'last_name' => 'Name',
            'role' => Permission::ROLE_ADMIN,
        ]));

        $this->artisan('db:seed', ['--class' => 'Motivo\\Liberiser\\Base\\Database\\Seeds\\LanguageSeeder']);
    }

    /** @test */
    public function create_visibility_test(): void
    {
        $locale = static::LOCALE_NL;

        $inputData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'visibility_visible' => $this->faker->boolean,
        ];

        $this->createMenu();
        $this->createPage(0, 1, $inputData);

        $this->assertDatabaseHas('liberiser_visibilities', ['visible' => $inputData['visibility_visible']]);
    }

    /** @test */
    public function update_visibility_test(): void
    {
        $locale = static::LOCALE_NL;

        $inputData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'visibility_visible' => 1,
        ];

        $this->createMenu();
        $page = $this->createPage(0, 1, $inputData);

        $updateData = [
            'title' => $this->faker->name,
            'locale' => $locale,
            'visibility_visible' => 0,
        ];

        $page = $this->updatePage($page, $updateData);

        $this->assertDatabaseMissing('liberiser_visibilities', ['visible' => $inputData['visibility_visible']]);
        $this->assertDatabaseHas('liberiser_visibilities', ['visible' => $updateData['visibility_visible']]);
    }

    /** @test */
    public function availability_per_language_test(): void
    {
        $inputData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => static::LOCALE_NL,
            'visibility_visible' => 1,
            'visibility_visible_languages' => [
                static::LOCALE_NL,
                static::LOCALE_EN,
            ],
        ];

        $this->createMenu();
        $page = $this->createPage(0, 1, $inputData);

        $updateData = $inputData;
        $updateData['locale'] = static::LOCALE_EN;

        $page = $this->updatePage($page, $updateData);

        $this->app = $this->createApplication();

        $link = '/'.Str::slug($inputData['title']);

        app()->setLocale(static::LOCALE_NL);
        $this->get($link)->assertOk();

        app()->setLocale(static::LOCALE_EN);
        $this->get($link)->assertOk();

        app()->setLocale('bs_locale');
        $this->get($link)->assertNotFound();
    }

    /** @test */
    public function availability_from_test(): void
    {
        $locale = static::LOCALE_NL;

        $dataWithoutFrom = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'visibility_visible' => 1,
        ];

        $dataFromPast = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'visibility_visible' => 1,
            'visibility_from' => Carbon::yesterday()->toDateString(),
        ];

        $dataFromFuture = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'visibility_visible' => 1,
            'visibility_from' => Carbon::tomorrow()->toDateString(),
        ];

        $this->createMenu();
        $this->createPage(0, 1, $dataWithoutFrom);
        $this->createPage(0, 1, $dataFromPast);
        $this->createPage(0, 1, $dataFromFuture);

        $this->assertEquals(Page::visible()->get()->count(), 2);
    }

    /** @test */
    public function availability_to_test(): void
    {
        $locale = static::LOCALE_NL;

        $dataWithoutFrom = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'visibility_visible' => 1,
        ];

        $dataUntilPast = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'visibility_visible' => 1,
            'visibility_until' => Carbon::yesterday()->toDateString(),
        ];

        $dataUntilFuture = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'visibility_visible' => 1,
            'visibility_until' => Carbon::tomorrow()->toDateString(),
        ];

        $this->createMenu();
        $this->createPage(0, 1, $dataWithoutFrom);
        $this->createPage(0, 1, $dataUntilPast);
        $this->createPage(0, 1, $dataUntilFuture);

        $this->assertEquals(Page::visible()->get()->count(), 2);
    }

    private function createPage(int $parentType, int $parentId, array $data): Page
    {
        $data = array_merge($data, ['parent_type' => $parentType, 'parent_id' => $parentId]);

        $this->post('/admin/page', $data);

        return Page::orderby('id', 'DESC')->first();
    }

    private function updatePage(Page $page, array $data): Page
    {
        $data = array_merge($data, ['id' => $page->id]);

        $this->put('/admin/page/'.$page->id, $data)->assertStatus(Response::HTTP_FOUND);

        return $page->refresh();
    }

    private function createMenu(int $amount = 1): void
    {
        for ($i = 0; $i < $amount; $i++) {
            $inputData = [
                'name' => $this->faker->name,
            ];

            $this->post('/admin/page/menu', $inputData)->assertStatus(Response::HTTP_FOUND);
        }
    }
}
