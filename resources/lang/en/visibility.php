<?php

return [
    'yes' => 'yes',
    'no' => 'no',
    'per_language' => 'per language',
    'from' => 'from :date',
    'until' => 'until :date',
    'columns' => [
        'visibility' => 'visible',
        'from' => 'visible from',
        'until' => 'visible until',
    ],
    'fields' => [
        'visibility' => 'visible',
        'from' => 'visible from',
        'until' => 'visible until',
    ],
];
