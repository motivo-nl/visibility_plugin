<?php

return [
    'yes' => 'ja',
    'no' => 'nee',
    'per_language' => 'per taal',
    'from' => 'vanaf :date',
    'until' => 'tot :date',
    'columns' => [
        'visibility' => 'zichtbaar',
        'from' => 'zichtbaar vanaf',
        'until' => 'zichtbaar tot',
    ],
    'fields' => [
        'visibility' => 'zichtbaar',
        'from' => 'zichtbaar vanaf',
        'until' => 'zichtbaar tot',
    ],
];
