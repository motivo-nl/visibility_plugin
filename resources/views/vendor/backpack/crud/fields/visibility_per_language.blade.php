@php
    $optionValue = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '';

    if (!isset($field['attributes']['class'])) {
        $field['attributes']['class'] = 'radio';
    }
@endphp

<div @include('crud::inc.field_wrapper_attributes') >
    <div>
        <label>{!! $field['label'] !!}</label>
        @include('crud::inc.field_translatable_icon')
    </div>

    <div class="radio">
        @php
            $optionType = 1; 

            if(isset($entry) && isset($entry->visibility) && $entry->visibility->visible && $entry->visibility->visible_languages) {
                $optionType = 2;
            }

            if (isset($entry) && isset($entry->visibility) && !$entry->visibility->visible) {
                $optionType = 0;
            }
        @endphp

        <label for="{{$field['name']}}_1">
            <input type="radio"
                   id="{{$field['name']}}_1"
                   name="{{$field['name']}}" value="1"
                   @if ($optionType === 1) checked @endif
            >
            {{ ucfirst(trans('LiberiserVisibility::visibility.yes')) }}
        </label>
        <label for="{{$field['name']}}_2">
            <input type="radio"
                   id="{{$field['name']}}_2"
                   name="{{$field['name']}}"
                   value="1"
                   @if ($optionType === 2) checked @endif
            >
            {{ ucfirst(trans('LiberiserVisibility::visibility.per_language')) }}
        </label>
        <label for="{{$field['name']}}_3">
            <input type="radio"
                   id="{{$field['name']}}_3"
                   name="{{$field['name']}}"
                   value="0"
                   @if ($optionType === 0) checked @endif
            >
            {{ ucfirst(trans('LiberiserVisibility::visibility.no')) }}
        </label>
    </div>

    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif

    <input type="hidden" name="visibility_visible_languages" value="null">
    <div class="languages" style="display: none">
        @if( isset($field['languages']))
            @foreach ($field['languages'] as $language => $label )
                @include('crud::inc.field_translatable_icon')
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="{{ $language }}" name="visibility_visible_languages[]"
                           @if(in_array($language, isset($entry) ? $entry->visibleLanguages() : []))
                               checked="checked"
                            @endif
                        > {!! $label !!}
                    </label>
                </div>
            @endforeach
        @endif
    </div>
</div>

@push('after_scripts')
    <script>
        $( document ).ready(function() {
            updateModulesDisplay();

            $('input:radio[name={{$field['name']}}]').change(function () {
                updateModulesDisplay();
            });

            function updateModulesDisplay() {
                $('.languages').hide();
                $( "input[type=checkbox][name='visibility_visible_languages[]']" ).prop( "disabled", true );

                if ($("input[name='{{$field['name']}}']:checked").attr('id') == "{{$field['name']}}_2") {
                    $( "input[name='visibility_visible_languages[]']" ).prop( "disabled", false );
                    $('.languages').show();
                }
            }
        });
    </script>
@endpush
