# Changelog

All notable changes to `liberiser-visibility` will be documented in this file

## 1.0.0 - 201X-XX-XX

- initial release
